"""
Running pysho modules directly
"""
from pysho.core import *

if WINDOWS:
    print("Is windows")
    print("USERNAME=" + ENV.USERNAME)
else:
    print("Isn't windows")
    print("HOME=" + ENV.HOME)

print("Globals:")
hr()
for item in sorted(dir()):
    if not item.startswith("_"):
        print(item)
