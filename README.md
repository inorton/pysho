# PySho (c) 2019-2020 Ian Norton

Pysho is intended to be a bare bones cross platform shell.  The motivation behind it is
to write simple shell-like scripts that work anywhere you can install python and use
the same file on windows, linux or more exotic systems.

## What is wrong with bash?

Bash is great, but it is not on all windows machines, nor all unix machines.  Pysho is
uniform on all platforms that support python3 so you need very little variation in your
scripts to support a multi-platform environment.

## What does it look like?

Pysho, as you might have guessed is basically python with a few helper functions.

```
print("Hello World!")
```

You run it simply by saving your file and doing:

```
C:\Users\inb\PycharmProjects\pysho>pysho examples\hello.pysh
Hello World!
```

As with most scripting, pysho lets you do many common operations, including:

* list files
* grep file contents
* make/delete folders
* touch files
* delete files
* use variables
* run programs
* capture program output

As pysho is python you can also use all of the normal python constructs:

```
try:
    print("about to run a bad program that exits non-zero..")
    if WINDOWS:
        capture(["dir", "nosuchthingfooblaaa"])
    else:
        capture(["/bin/false"])
except:
    print("process call failed!")
finally:
    print("finally...")
```
Which prints:
```
about to run a bad program that exits non-zero..
process call failed!
finally...
```

And as it is mostly normal python, you can use variables:

```
paths = ENV.PATH.split(os.pathsep)
foo = paths[0] + os.pathsep + os.getcwd()
print(foo)
```

Which might print something like:

```
python3 -m pysho examples/vars.pysho
/usr/local/bin:/home/inb/Projects/python/pysho
```

# Installation

You can usually install from pypi using pip
```
pip install pysho
```

You can also install from source using pip

```
cd pysho
python setup.py sdist
cd dist
pip install *.*
```

## Supported Systems

Pysho should run on anything that supports python 3.6 or later.  Right now it works on python 2 on Windows and Linux but
Python 2 support will not continue should new changes break it.

## Windows Integration

Pysho can be set to execute `.pysh` files (run under an administrator command prompt):

```
pysho --register
```

You should then just be able to run the scripts directly:

```
C:\Users\inb\PycharmProjects\pysho\examples>hello.pysh
Hello World!
```
